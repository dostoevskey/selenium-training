package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

/**
 * The type Page object.
 */
public class PageObject {

    /**
     * The Page title.
     */
    @FindBy(css = "td.content h1")
    @CacheLookup
    public WebElement pageTitle;

    /**
     * The Result message.
     */
    @FindBy(css = "p#result")
    @CacheLookup
    public WebElement resultMessage;

    /**
     * The Success message.
     */
    @FindBy(css = ".content h1")
    @CacheLookup
    public WebElement successMessage;

    /**
     * The Driver.
     */
    protected WebDriver driver;

    /**
     * Instantiates a new Page object.
     *
     * @param driver the driver
     */
    public PageObject(final WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    /**
     * Return title string.
     *
     * @return the string
     */
    public String returnTitle() {
        return driver.getTitle();
    }

    /**
     * Return message string.
     *
     * @return the string
     */
    public String returnMessage() {
        return resultMessage.getText();
    }

    /**
     * Return successful message string.
     *
     * @return the string
     */
    public String returnSuccessfulMessage() {
        return successMessage.getText();
    }


    /**
     * Open page.
     *
     * @param pageName the page name
     */
    public void openPage(final String pageName) {
        driver.get("http://test-engineer.info/testroom/" + pageName);
    }

    /**
     * Assert alert message.
     *
     * @param alertMessage the alert message
     */
    public void assertAlertMessage(final String alertMessage) {
        Assert.assertEquals(alertMessage, driver.switchTo().alert().getText());
    }

    /**
     * Sets page title.
     *
     * @param pageTitle the page title
     */
    public void setPageTitle(WebElement pageTitle) {
        this.pageTitle = pageTitle;
    }
}
