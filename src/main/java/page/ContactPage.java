package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * The type Contact page.
 */
public class ContactPage extends PageObject {

    @FindBy(name = "name_field")
    @CacheLookup
    private WebElement enterNameField;

    @FindBy(name = "address_field")
    @CacheLookup
    private WebElement enterAddressField;

    @FindBy(name = "postcode_field")
    @CacheLookup
    private WebElement enterPostCodeField;

    @FindBy(name = "email_field")
    @CacheLookup
    private WebElement enterEmailField;

    @FindBy(css = "#rinfo")
    @CacheLookup
    private WebElement informationRadioButton;

    @FindBy(css = "#rdona")
    @CacheLookup
    private WebElement donationRadioButton;

    @FindBy(css = "#radop")
    @CacheLookup
    private WebElement testingRadioButton;

    @FindBy(css = "#cdona")
    @CacheLookup
    private WebElement checkBoxVolunteer;

    @FindBy(css = "#cadop")
    @CacheLookup
    private WebElement checkBoxEmailNews;

    /**
     * The Submit message button.
     */
    @FindBy(css = "#submit_message")
    @CacheLookup
    public WebElement submitMessageButton;

    /**
     * Instantiates a new Contact Us.
     *
     * @param driver the driver
     */
    public ContactPage(final WebDriver driver) {
        super(driver);
    }

    private WebDriverWait webDriverWait = new WebDriverWait(driver,5);

    /**
     * Fill out contact form.
     *
     * @param name     the name
     * @param address  the address
     * @param postCode the post code
     * @param email    the email
     */
    public void fillOutContactForm(String name, String address, String postCode, String email) {
        enterNameField.sendKeys(name);
        enterAddressField.sendKeys(address);
        enterPostCodeField.sendKeys(postCode);
        enterEmailField.sendKeys(email);
        testingRadioButton.click();
        checkBoxVolunteer.click();
    }

    /**
     * Wait until web element is visible.
     *
     * @param WebElementId the web element id
     */
    public void waitUntilWebElementIsVisible(final WebElement WebElementId) {
        webDriverWait.until(ExpectedConditions.visibilityOf(WebElementId));
    }
}
