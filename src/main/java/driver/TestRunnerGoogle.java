package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import utils.PropertiesReader;

/**
 * The type Test runner.
 */
public class TestRunnerGoogle extends PropertiesReader {

    /**
     * The Driver.
     */
    protected WebDriver driver;

    /**
     * Instantiates a new Test runner google.
     */
    public TestRunnerGoogle() {
        super();
        //empty
        return;
    }

    /**
     * Start browser.
     */
    @BeforeTest(alwaysRun = true)
    public void startBrowser() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().setSize(new Dimension(1300, 768));
        driver.get(loadProperty("urlGoogle"));
    }

    /**
     * Delete all cookies.
     */
    @AfterTest(alwaysRun = true)
    public void deleteAllCookies() {
        if (driver != null) {
            driver.manage().deleteAllCookies();
        }
    }

    /**
     * Teardown.
     */
    @AfterTest(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}

