package selenium;

import driver.TestRunnerGoogle;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.GoogleSearch;

public class TypeSeleniumTestGoogle extends TestRunnerGoogle {

    @Test(description = "Validate of Selenium output !=9")
    public void testOpenGooglePageAndTypeSelenium() {
        GoogleSearch googleSearch = new GoogleSearch(driver);
        googleSearch.searchByWord("Selenium");
        googleSearch.waitUntilAllPageElementsAreLoaded();
        Assert.assertNotEquals(9, googleSearch.returnListOfOutput());
    }


}


