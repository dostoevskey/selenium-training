package sendemailcontactpage;

import driver.TestRunnerTestEngineer;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.ContactPage;

public class SendEmail extends TestRunnerTestEngineer {

    @Test(description = "Send message to admin on CONTACT page")
    public void sendMessageToAdmin() {

        ContactPage contactPage = new ContactPage(driver);

        contactPage.openPage("contact.html");
        Assert.assertEquals("The Test Room | Contact Page", contactPage.returnTitle());
        contactPage.fillOutContactForm("Test User", "1000 Main street",
                "98010", "testEmail@test.com");
        contactPage.submitMessageButton.click();
        contactPage.waitUntilWebElementIsVisible(contactPage.pageTitle);
        Assert.assertEquals("We have your message",contactPage.returnSuccessfulMessage());
    }
}