package sendemailcontactpagenegative;

import driver.TestRunnerTestEngineer;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.ContactPage;

public class SendEmailNegative extends TestRunnerTestEngineer {

    @Test(description = "Negative scenario to send message to admin on CONTACT page without any data")
    public void sendMessageToAdminNegative() {

        ContactPage contactPage = new ContactPage(driver);

        contactPage.openPage("contact.html");
        Assert.assertEquals("The Test Room | Contact Page", contactPage.returnTitle());
        contactPage.submitMessageButton.click();
        Assert.assertEquals("Name field is empty", driver.switchTo().alert().getText());
        driver.switchTo().alert().accept();
    }
}
