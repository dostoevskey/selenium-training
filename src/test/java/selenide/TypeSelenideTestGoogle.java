package selenide;

import driver.TestRunnerGoogle;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.GoogleSearch;

public class TypeSelenideTestGoogle extends TestRunnerGoogle {

    @Test(description = "Validate of Selenide output =9")
    public void testOpenGooglePageAndTypeSelenide() {
        GoogleSearch googleSearch = new GoogleSearch(driver);
        googleSearch.searchByWord("Selenide");
        googleSearch.waitUntilAllPageElementsAreLoaded();
        Assert.assertEquals(9, googleSearch.returnListOfOutput());
    }
}
