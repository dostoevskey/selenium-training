package checkone;

import driver.TestRunnerTestEngineer;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.TestPage;

/**
 * The type Check 1 test.
 */
public class CheckOneButton extends TestRunnerTestEngineer {

    @Test(description = "Select dropdown list and click check1 button")
    public void selectAndClickCheck1() {
        TestPage testPage = new TestPage(driver);

        testPage.openPage("testing.html");
        Assert.assertEquals("The Test Room | Test Page", testPage.returnTitle());
        testPage.selectDay("Yesterday");
        testPage.selectButton("Check 1");
        Assert.assertEquals("SORRY, NOT AVAILABLE", testPage.returnMessage());
    }
}
